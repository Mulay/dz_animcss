'use strict';

const gulp = require('gulp');

function lazyRequireTask(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, function(callback) {
        let task = require(path).call(this, options);

        return task(callback);
    });
}

lazyRequireTask('styles', './modules/styles', {
    src: 'src/styles/main.scss'
});

lazyRequireTask('clean', './modules/clean', {
    dst: 'public'
});

lazyRequireTask('assets', './modules/assets', {
    src: 'src/assets/**',
    dst: 'public'
});

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('styles', 'assets'))
);

gulp.task('watch', function() {
    gulp.watch('src/styles/**/*.*', gulp.series('styles'));

    gulp.watch('src/assets/**/*.*', gulp.series('assets'));
});

lazyRequireTask('serve', './modules/serve', {
    src: 'public'
});

gulp.task('dev',
    gulp.series('build', gulp.parallel('watch', 'serve'))
);

lazyRequireTask('sprite', './modules/sprite', {
    src: 'src/sprite/*.*'
});